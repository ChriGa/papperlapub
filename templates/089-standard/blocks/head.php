<?php
/**
 * @author   	089webdesgin.de
 * @copyright   Copyright (C) 2015 089webdesgin.de. All rights reserved.
 * @URL 		https://089webdesgin.de/
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */


defined('_JEXEC') or die;

// Add JavaScript Frameworks
JHtml::_('bootstrap.framework');

// Add Stylesheets
//$doc->addStyleSheet('templates/' . $this->template . '/css/normalize.css');
//$doc->addStyleSheet('templates/' . $this->template . '/css/responsive.css');
	

// Load optional RTL Bootstrap CSS
JHtml::_('bootstrap.loadCss', true, $this->direction);

// Add Stylesheets - CG overrides als letztes!!
//$doc->addStyleSheet('templates/' . $this->template . '/css/overrides.css');

?>

	<meta name="viewport" content="width=device-width, initial-scale=1.0" />
	<jdoc:include type="head" />
	<?php // Use of Google Font ?>
	<?php if ($this->params->get('googleFont')) : ?>
		<link href='//fonts.googleapis.com/css?family=<?php echo $this->params->get('googleFontName'); ?>' rel='stylesheet' type='text/css' />
		<style type="text/css">
			h1,h2,h3,h4,h5,h6,.site-title{
				font-family: '<?php echo str_replace('+', ' ', $this->params->get('googleFontName')); ?>', sans-serif;
			}
		</style>
	<?php endif; ?>

	<link rel="canonical" href="<?php print JURI::current(); ?>" />

	<!--[if lt IE 9]>
		<script src="<?php echo $this->baseurl; ?>/media/jui/js/html5.js"></script>
	<![endif]-->

<?php // Bugherd: ?>
<script type="text/javascript" src="https://www.bugherd.com/sidebarv2.js?apikey=zxmkfnud8pdb6j1ybepylg" async="true"></script>