<?php
/**
 * @author   	089webdesgin.de
 * @copyright   Copyright (C) 2019 089webdesgin.de. All rights reserved.
 * @URL 		https://089webdesgin.de/
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;
?>
<footer class="footer">
	<?php /*
	<div class="footer-wrap">				
		<?php if ($this->countModules('footnav')) : ?>
			<div class="row-fluid">								
				<div class="span8 footnav">
					<div class="module_footer position_footnav">
						<jdoc:include type="modules" name="footnav" style="custom" />
					</div>			
				</div>
			</div>		
		<?php endif ?>
		<?php if($this->countModules('footer')) : ?>
			<div class="row-fluid">				
				<div class="span12 footer--module">
					<jdoc:include type="modules" name="footer" style="custom" />
				</div>
			</div>
		<?php endif;?>
		<div class="footer--bottom">		
			<div class="logo--small__footer">
				<a class="brand" href="<?php echo $this->baseurl; ?>">
					<?php echo $logo; ?>
					<?php if ($this->params->get('sitedescription')) : ?>
						<?php echo '<div class="site-description">' . htmlspecialchars($this->params->get('sitedescription')) . '</div>'; ?>
					<?php endif; ?>
				</a>
			</div>
		    <div class="vcard"> 
		        <p class="org">PapperlaPUB</p>
		        <p class="tel "><a class="" href="tel:+49-555-perfect">Tel: +49 - (0) 8171 - 3855355</a></p>
		        <p class="adr"><span class="street-address">Hirschenweg 1</span><br />
		          <span class="postal-code">82538 </span><span class="region">Geretsried</span>
		         </p>
		    </div>	
	   	</div>	
	</div> */?>
	<div id="copyright" class="fullwidth">
		<div class="copyWrapper">
			<p> <a class="imprLink uppercase" href="/impressum.html" title="Impressum FIRMA">Impressum</a> | <a class="imprLink uppercase" href="/datenschutz.html">Datenschutz</a></p>
			<p>&copy; <?php print date("Y") . " " . $sitename; ?></p>
		</div>
	</div>	
</footer>
		