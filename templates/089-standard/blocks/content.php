<?php
/**
 * @author   	089webdesgin.de
 * @copyright   Copyright (C) 2015 089webdesgin.de. All rights reserved.
 * @URL 		https://089webdesgin.de/
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;
?>

<div class="content <?php print($frontpage) ? " " : "innerwidth" ?>">
	<jdoc:include type="modules" name="banner" style="xhtml" />	

	<main id="content" role="main" class="main-content">
	<!-- Begin Content -->
	<jdoc:include type="modules" name="position-3" style="xhtml" />
	<jdoc:include type="message" />
	<jdoc:include type="component" />
	<jdoc:include type="modules" name="position-2" style="none" />
	<!-- End Content -->
	</main>
	
	<?php if ($this->countModules('right')) : ?>
		<div class="span3 right">		
			<jdoc:include type="modules" name="right" style="xhtml" />
		</div>
	<?php endif; ?>
</div>