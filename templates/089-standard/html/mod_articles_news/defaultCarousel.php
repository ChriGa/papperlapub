<?php
/**
 * @package     089 NewsFlash MOD
 * @subpackage  mod_articles_news MOD Chris Gabler für PPB
 *
 * @copyright   Copyright (C) 2005 - 2019 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;
?>
<div class="newsflash<?php echo $moduleclass_sfx; ?>">
	<div id="myCarousel" class="carousel-fade carousel slide">
		<ol class="carousel-indicators">
		<?php 
			$x = 0;
			while($list[$x]) {
				print '<li data-target="#myCarousel" data-slide-to="'.$x.'" class=""></li>';
				$x++;
			}
		?>
		</ol>		
		<div class="carousel-inner">
			<?php foreach ($list as $item) : ?>
				<div class="item">			
					<?php require JModuleHelper::getLayoutPath('mod_articles_news', '_item'); ?>
				</div>
			<?php endforeach; ?>
		</div>		
	</div>
</div>
<script type="text/javascript">
	jQuery(function(){
		
		jQuery('.carousel-inner div:first').addClass('active');

		jQuery('.carousel').carousel({
			interval: 4000
		});
	});
</script>